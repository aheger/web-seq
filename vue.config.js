const isProd = process.env.NODE_ENV === 'production';
const CompressionWebpackPlugin = require('compression-webpack-plugin');

module.exports = {
	baseUrl: process.env.BASE_URL || '/',
	productionSourceMap: false,

	css: {
		loaderOptions: {
			sass: {
				data: `@import "@/_vars.scss";`
			}
		}
	},

	configureWebpack: (config) => {
		if (isProd) {
			config.plugins.push(new CompressionWebpackPlugin());
		}
	},

	pluginOptions: {
		i18n: {
			locale: 'en',
			fallbackLocale: 'en',
			localeDir: 'locales',
			enableInSFC: true
		}
	}
};
