import Transport from '@/midi-scheduler/transport';
import Track from '@/midi-scheduler/track';
import Step from '@/midi-scheduler/step';

const DEBUG_ID = localStorage.getItem('last_output') || '';

const transport = new Transport();

transport.tracks = [
	new Track({ outputId: DEBUG_ID, channel: 10, steps: [
		new Step({ pitch: 36, active: true }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36, active: true }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
		new Step({ pitch: 36 }),
	]}),
	new Track({ outputId: DEBUG_ID, channel: 10, steps: [
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38, active: true}),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38, active: true}),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
		new Step({ pitch: 38 }),
	]}),
	new Track({ outputId: DEBUG_ID, channel: 10, steps: [
		new Step({ pitch: 42, active: true}),
		new Step({ pitch: 42 }),
		new Step({ pitch: 42, active: true}),
		new Step({ pitch: 42 }),
		new Step({ pitch: 42, active: true}),
		new Step({ pitch: 42 }),
		new Step({ pitch: 42, active: true}),
		new Step({ pitch: 42 }),
		new Step({ pitch: 42, active: true}),
		new Step({ pitch: 42 }),
		new Step({ pitch: 42, active: true}),
		new Step({ pitch: 42 }),
		new Step({ pitch: 42, active: true}),
		new Step({ pitch: 42 }),
		new Step({ pitch: 42, active: true}),
		new Step({ pitch: 42 }),

	]}),
	new Track({ outputId: DEBUG_ID, channel: 1, steps: [
		new Step({ pitch: 24, active: true, duration: 4 }),
		new Step({ }),
		new Step({ }),
		new Step({ pitch: 25 }),
		new Step({ pitch: 25, active: true }),
		new Step({ pitch: 24, active: true, duration: 3 }),
		new Step({ }),
		new Step({ }),
		new Step({ pitch: 24, active: true }),
		new Step({ }),
		new Step({ pitch: 24, active: true }),
		new Step({ }),
		new Step({ pitch: 25, active: true, duration: 3 }),
		new Step({ }),
		new Step({ }),
		new Step({ }),
	]}),
];

export default transport;
