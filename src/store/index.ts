import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { IRootState } from './types';
import midi from './midi';
import { version } from '../../package.json';

Vue.use(Vuex);

const store: StoreOptions<IRootState> = {
	state: {
		version,
	},
	modules: {
		midi,
	},
};

export default new Vuex.Store<IRootState>(store);
