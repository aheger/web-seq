import { Module } from 'vuex';

import getters from './getters';
import actions from './actions';
import mutations from './mutations';

import { IMidiState, IStepMap, ITrackMap, FeatureState } from './types';
import { IRootState } from '../types';

import transport from '@/debug/transport';

const tracksById: ITrackMap = { };
const stepsById: IStepMap = { };

for (const track of transport.tracks) {
	tracksById[track.id] = track;

	for (const step of track.steps) {
		stepsById[step.id] = step;
	}
}

export const state: IMidiState = {
	feature: FeatureState.Unresolved,
	outputs: new Map(),
	transport,
	tracksById,
	stepsById,
};

const namespaced: boolean = true;

const midi: Module<IMidiState, IRootState> = {
	namespaced,
	state,
	getters,
	actions,
	mutations,
};

export default midi;
