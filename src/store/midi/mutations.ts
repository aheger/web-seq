import { MutationTree } from 'vuex';
import { FeatureState, IMidiState, Mutations } from './types';
import { IStep, ITrack } from '@/midi-scheduler/types';

import Track from '@/midi-scheduler/track';
import Step from '@/midi-scheduler/step';

const mutations: MutationTree<IMidiState> = {
	[Mutations.OUTPUTS](state, payLoad: WebMidi.MIDIOutputMap) {
		state.outputs = payLoad;
	},
	[Mutations.FEATURE](state, payLoad: FeatureState) {
		state.feature = payLoad;
	},
	[Mutations.ADD_STEP](state, { id }: { id: string }) {
		const track = state.tracksById[id];

		if (!track) {
			return;
		}

		const prevStep = track.steps[track.steps.length - 1];

		const step = new Step();

		if (prevStep) {
			step.pitch = prevStep.pitch;
		}

		state.stepsById = Object.assign({}, state.stepsById, {[step.id]: step});

		track.steps.push(step);
	},
	[Mutations.REMOVE_STEP](state, { id }: { id: string }) {
		const track = state.tracksById[id];

		if (!track) {
			return;
		}

		// never remove the last step
		if (track.steps.length === 1) {
			return;
		}

		const step = track.steps.pop()!;
		const steps = state.stepsById;

		delete steps[step.id];

		state.stepsById = steps;
	},
	[Mutations.STEP](state, payLoad: IStep) {
		state.stepsById = Object.assign({}, state.stepsById, {[payLoad.id]: payLoad});

		// TODO: find simpler solution for determining the track of the step
		for (const track of state.transport.tracks) {
			const ix = track.steps.findIndex((step) => step.id === payLoad.id);

			if (ix === -1) {
				continue;
			}

			track.steps.splice(ix, 1, payLoad);

			break;
		}
	},
	[Mutations.ADD_TRACK](state) {
		const { transport } = state;
		const prevTrack = transport.tracks[transport.tracks.length - 1];

		const settings = prevTrack || { outputId: '', channel: 1};

		let steps;
		if (prevTrack) {
			steps = prevTrack.steps.map((s) => {
				return new Step({
					pitch: s.pitch,
					velocity: s.velocity,
				});
			});
		} else {
			steps = [
				new Step(),
				new Step(),
				new Step(),
				new Step(),
			];
		}

		for (const step of steps) {
			state.stepsById[step.id] = step;
		}

		const track = new Track({
			outputId: settings.outputId,
			channel: settings.channel,
			steps,
		});

		transport.tracks.push(track);

		state.tracksById[track.id] = track;

	},
	[Mutations.REMOVE_TRACK](state, payLoad: ITrack) {
		const track = state.tracksById[payLoad.id];

		if (!track) {
			return;
		}

		const tracksById = state.tracksById;

		delete tracksById[payLoad.id];

		state.tracksById = tracksById;

		const { transport: { tracks } } = state;
		tracks.splice(tracks.indexOf(track), 1);
	},
	[Mutations.TRACK](state, payLoad: { id: string, channel?: number, outputId?: string }) {
		const track = state.tracksById[payLoad.id];

		if (!track) {
			return;
		}

		if (payLoad.channel) {
			track.channel = payLoad.channel;
		}

		if (payLoad.outputId) {
			track.outputId = payLoad.outputId;
		}

		state.tracksById = Object.assign({}, state.tracksById, {[payLoad.id]: track});

		const tracks = state.transport.tracks;

		tracks.splice(tracks.indexOf(track), 1, track);
	},
};

export default mutations;
