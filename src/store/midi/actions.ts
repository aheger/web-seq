import { ActionTree } from 'vuex';
import { Actions, FeatureState, IMidiState, Mutations } from './types';
import { IRootState } from '../types';

const actions: ActionTree<IMidiState, IRootState> = {
	async [Actions.INIT]({ commit, dispatch }) {
		if (!navigator.requestMIDIAccess) {
			commit(Mutations.FEATURE, FeatureState.Unsupported);
			return;
		}

		try {
			const access = await navigator.requestMIDIAccess();
			access.onstatechange = () => {
				dispatch(Actions.CHECK_ACCESS, access);
			};

			dispatch(Actions.CHECK_ACCESS, access);
			commit(Mutations.FEATURE, FeatureState.Supported);
		} catch (ex) {
			commit(Mutations.FEATURE, FeatureState.Forbidden);
		}
	},
	[Actions.CHECK_ACCESS]({ commit }, access) {
		commit(Mutations.OUTPUTS, access.outputs);
	},
	[Actions.ACTIVATE_STEP]({ commit }, payLoad) {
		commit(Mutations.STEP, payLoad);
	},
	[Actions.ADD_STEP]({ commit }, payLoad) {
		commit(Mutations.ADD_STEP, payLoad);
	},
	[Actions.REMOVE_STEP]({ commit }, payLoad) {
		commit(Mutations.REMOVE_STEP, payLoad);
	},
	[Actions.UPDATE_STEP]({ commit }, payLoad) {
		commit(Mutations.STEP, payLoad);
	},
	[Actions.ADD_TRACK]({ commit }, payLoad) {
		commit(Mutations.ADD_TRACK, payLoad);
	},
	[Actions.REMOVE_TRACK]({ commit }, payLoad) {
		commit(Mutations.REMOVE_TRACK, payLoad);
	},
	[Actions.UPDATE_TRACK]({ commit }, payLoad) {
		commit(Mutations.TRACK, payLoad);
	},
};

export default actions;
