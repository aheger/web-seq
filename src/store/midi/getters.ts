import { GetterTree } from 'vuex';
import { IMidiState, Getters } from './types';
import { IRootState } from '../types';

function sortByName(a: WebMidi.MIDIOutput, b: WebMidi.MIDIOutput) {
	return (a.name || '').localeCompare(b.name || '');
}

export const getters: GetterTree<IMidiState, IRootState> = {
	[Getters.DEBUG](_state, stateGetters): string {
		const outputs = stateGetters[Getters.OUTPUT_LIST];
		if (!outputs.length) {
			return '';
		}

		return `out: ${outputs.length}`;
	},
	[Getters.STEP_LENGTH]({ transport }): number {
		const stepCounts = transport.tracks.map((t) => t.steps.length);

		return Math.max(...stepCounts);
	},
	[Getters.OUTPUT_LIST]({ outputs }): WebMidi.MIDIOutput[] {
		return Array.from(outputs.values()).sort(sortByName);
	},
};

export default getters;
