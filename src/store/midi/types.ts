import { ITransport, ITrack, IStep } from '@/midi-scheduler/types';

export enum Actions {
	INIT = 'init',
	CHECK_ACCESS = 'check_access',
	ACTIVATE_STEP = 'activate_step',
	ADD_STEP = 'add_step',
	REMOVE_STEP = 'remove_step',
	UPDATE_STEP = 'update_step',
	ADD_TRACK = 'add_track',
	REMOVE_TRACK = 'remove_track',
	UPDATE_TRACK = 'update_track',
}

export enum Getters {
	DEBUG = 'debug',
	OUTPUT_LIST = 'output_list',
	STEP_LENGTH = 'step_length',
}

export enum Mutations {
	ADD_STEP = 'add_step',
	FEATURE = 'feature',
	OUTPUTS = 'outputs',
	REMOVE_STEP = 'remove_step',
	STEP = 'step',
	ADD_TRACK = 'add_track',
	REMOVE_TRACK = 'remove_track',
	TRACK = 'track',
}

export enum FeatureState {
	Unresolved,
	Supported,
	Unsupported,
	Forbidden,
}

export interface IMidiState {
	feature: FeatureState;
	outputs: Map<string, WebMidi.MIDIOutput>;
	transport: ITransport;
	tracksById: ITrackMap;
	stepsById: IStepMap;
}

export interface IStepMap {
	[key: string]: IStep;
}

export interface ITrackMap {
	[key: string]: ITrack;
}
