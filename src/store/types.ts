import { IMidiState } from './midi/types';

export interface IRootState {
	version: string;
	midi?: IMidiState;
}
