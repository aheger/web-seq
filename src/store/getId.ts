const ids: { [key: string]: number } = {};

export default function getId(namespace?: string): string {
	namespace = namespace || 'default';

	if (!ids[namespace]) {
		ids[namespace] = 0;
	}

	return `${namespace}_${++ids[namespace]}`;
}
