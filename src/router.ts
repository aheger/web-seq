import Vue from 'vue';
import Router, { Route } from 'vue-router';

import About from './views/About.vue';
import Home from './views/Home.vue';
import Init from './views/Init.vue';

import store from '@/store';
import { FeatureState } from '@/store/midi/types';

Vue.use(Router);

function requiresInit(route: Route) {
	if (route.name === 'init') {
		return false;
	}

	if (route.name === 'about') {
		return false;
	}

	return store.state.midi!.feature === FeatureState.Unresolved;
}

const router =  new Router({
	mode: process.env.VUE_APP_PUSH_STATE === '1' ? 'history' : 'hash',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home,
		},
		{
			path: '/about',
			name: 'about',
			component: About,
		},
		{
			path: '/init',
			name: 'init',
			component: Init,
		},
	],
});

router.beforeEach((to, _from, next) => {
	if (requiresInit(to)) {
		return next({ name: 'init' });
	}
	next();
});

export default router;
