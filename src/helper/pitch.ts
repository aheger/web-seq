// define supported MIDI pitches
const pitches = Array.from({length: 83}, (_, k) => k + 24);

const noteNames = ['c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#', 'a', 'a#', 'b'];

interface INameByPitch {
	[key: number]: string;
}

const notes: Array<{ name: string, pitch: number }> = pitches.map((pitch) => {
	const nameIndex = (pitch - 24) % noteNames.length;
	const nameNo = Math.floor((pitch - 24) / noteNames.length);

	return {
		pitch,
		name: `${noteNames[nameIndex]}-${nameNo}`,
	};
});

export const nameByPitch: INameByPitch = notes.reduce((agg: INameByPitch, n) => {
	agg[n.pitch] = n.name;

	return agg;
}, {});

export default notes;
