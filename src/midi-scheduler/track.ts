import { ITrack, IStep } from './types';
import getId from '@/store/getId';

export default class Track implements ITrack {
	private _id: string = '';
	private _outputId: string = '';
	private _channel: number = 0;

	private _stepCursor: number = 0;
	private _steps: IStep[] = [];

	constructor(data: { id?: string, outputId: string, channel: number, steps: IStep[] }) {
		this._outputId = data.outputId;
		this._channel = data.channel;
		this._steps = data.steps;

		if (data.id) {
			this._id = data.id;
			return;
		}

		this._id = getId('track');
	}

	get id(): string {
		return this._id;
	}

	get stepCursor(): number {
		return this._stepCursor;
	}

	get outputId(): string {
		return this._outputId;
	}

	set outputId(val: string) {
		this._outputId = val;
	}

	get channel(): number {
		return this._channel;
	}

	set channel(val: number) {
		this._channel = val;
	}

	get steps(): IStep[] {
		return this._steps;
	}

	step(): IStep {
		const lastStep = this._steps[this._stepCursor];

		if (lastStep) {
			this._stepCursor = (this._stepCursor + 1) % this._steps.length;
			return lastStep;
		}

		// reset overflow step cursor
		this._stepCursor = 0;
		return this._steps[0];
	}

	resetStep() {
		this._stepCursor = 0;
	}
}
