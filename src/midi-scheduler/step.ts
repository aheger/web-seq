import { IStep } from './types';
import getId from '@/store/getId';

export default class Step implements IStep {
	pitch: number = 60;

	velocity: number = 100;

	active: boolean = false;

	duration: number = 1;

	private _id: string = '';

	get id(): string {
		return this._id;
	}

	constructor(data: { id?: string, pitch?: number, velocity?: number, active?: boolean, duration?: number } = {}) {
		if (data) {
			this.active = !!data.active;

			if (data.pitch) {
				this.pitch = data.pitch;
			}

			if (data.velocity) {
				this.velocity = data.velocity;
			}

			if (data.duration) {
				this.duration = data.duration;
			}

			if (data.id) {
				this._id = data.id;
			}
		}

		if (!this._id) {
			this._id = getId('step');
		}
	}
}
