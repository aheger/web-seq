// tslint:disable:no-bitwise

export enum SystemMessages {
	// System common messages
	SONGPOSITION = 0xf2,
	SONGSELECT = 0xf3,
	SYSEX = 0xf0,
	SYSEXEND = 0xf7,
	TIMECODE = 0xf1,
	TUNINGREQUEST = 0xf6,

	// SYSTEM REAL-TIME MESSAGES
	ACTIVESENSING = 0xfe,
	CLOCK = 0xf8,
	CONTINUE = 0xfb,
	RESET = 0xff,
	START = 0xfa,
	STOP = 0xfc,
}

export enum ChannelMessages {
	CHANNELAFTERTOUCH = 0xd,
	CHANNELMODE = 0xb,
	CONTROLCHANGE = 0xb,
	KEYAFTERTOUCH = 0xa,
	NOTEOFF = 0x8,
	NOTEON = 0x9,
	PITCHBEND = 0xe,
	PROGRAMCHANGE = 0xc,
}

export enum ChannelModeMessages {
	ALLSOUNDOFF = 120,
	RESETALLCONTROLLERS = 121,
	LOCALCONTROL= 122,
	ALLNOTESOFF= 123,
	OMNIMODEOFF= 124,
	OMNIMODEON= 125,
	MONOMODEON= 126,
	POLYMODEON= 127,
}

export default abstract class MidiHelper {
	static playNote(
		out: WebMidi.MIDIOutput,
		channel: number,
		note: { pitch: number, velocity: number, duration: number },
		timestamp: DOMHighResTimeStamp = window.performance.now(),
	) {
		out.send(
			[
				(ChannelMessages.NOTEON << 4) + (channel - 1),
				note.pitch,
				note.velocity,
			],
			timestamp,
		);
		out.send(
			[
				(ChannelMessages.NOTEOFF << 4) + (channel - 1),
				note.pitch,
				note.velocity,
			],
			timestamp + note.duration,
		);

		return this;
	}

	static sendAllNotesOff(
		out: WebMidi.MIDIOutput,
		channel: number,
	) {
		out.send([
			(ChannelMessages.CHANNELMODE << 4) + (channel - 1),
			ChannelModeMessages.ALLNOTESOFF,
			0,
		]);

	}
}
