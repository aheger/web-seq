export interface IUnique {
	id: string;
}

export interface ITransport {
	bpm: number;
	divider: number;
	tracks: ITrack[];
}

export interface ITrack extends IUnique {
	outputId: string;
	channel: number;
	stepCursor: number;
	step: () => IStep;
	steps: IStep[];
	resetStep: () => void;
}

export interface IStep extends IUnique {
	pitch: number;
	velocity: number;
	active: boolean;
	duration: number;
}
