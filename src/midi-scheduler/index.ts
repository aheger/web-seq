import MidiHelper from './helper';
import { ITransport } from './types';

import store from '@/store';

const LOOKAHEAD = 50;

interface IBoundChannel {
	outputId: string;
	channel: number;
}

export default class MidiScheduler {
	private _transport: ITransport;
	private _outputs: Map<string, WebMidi.MIDIOutput>;

	private _timeout: number | null = null;

	private _cursorTimestamp: number = 0;

	private _touched: { [key: string]: IBoundChannel } = {};

	constructor(transport: ITransport) {
		this._transport = transport;
		this._outputs = store.state.midi!.outputs;
	}

	get playing() {
		return this._timeout !== null;
	}

	private get stepDuration() {
		const bpm = isNaN(this._transport.bpm) ? 20 : this._transport.bpm;
		const divider = isNaN(this._transport.divider) ? 4 : this._transport.divider;

		return (60000 / bpm) / divider;
	}

	start() {
		if (this._timeout) {
			return;
		}

		this._cursorTimestamp = performance.now();

		const worker = () => {
			if (performance.now() - this._cursorTimestamp >= LOOKAHEAD) {
				this.advanceCursor();
			}
			this._timeout = window.requestAnimationFrame(worker);
		};

		worker();
	}

	stop() {
		if (this._timeout) {
			window.cancelAnimationFrame(this._timeout);
			this._timeout = null;
		}

		this.stopAll();
	}

	private advanceCursor() {
		const now = performance.now();

		// skip all missed steps since last pulse
		if (now - this._cursorTimestamp > 2 * this.stepDuration) {
			this.moveCursor(now - LOOKAHEAD, true);
		}

		// return steps that still need to be played
		this.moveCursor(now + LOOKAHEAD);
	}

	private moveCursor(target: number, skip: boolean = false) {
		const stepDuration = this.stepDuration;

		while (this._cursorTimestamp < target) {
			this._cursorTimestamp += stepDuration;

			// inverse loop for performance
			for (let index = this._transport.tracks.length - 1; index >= 0; index--) {
				const t = this._transport.tracks[index];
				const trackStep = t.step();

				if (skip || !trackStep) {
					continue;
				}

				if (!trackStep.pitch || !trackStep.active) {
					continue;
				}

				const o = this._outputs.get(t.outputId);

				if (!o) {
					continue;
				}

				// calculate for how many steps this step will ring until it is
				// either ended by its own duration or another step beginning
				let stepSpan: number = 1;
				for (; stepSpan < trackStep.duration; stepSpan++) {
					if (t.steps[(t.stepCursor + stepSpan - 1) % t.steps.length].active) {
						break;
					}
				}

				MidiHelper.playNote(
					o,
					t.channel,
					{
						pitch: trackStep.pitch,
						velocity: trackStep.velocity,
						// 5ms timing safety for ending the note without interrupting the same note
						// TODO: find more elegant solution
						duration: stepSpan * stepDuration - 5,
					},
					this._cursorTimestamp,
				);

				// remember we touched this channel
				this._touched[o.id + '_' + t.channel] = { outputId: o.id, channel: t.channel };
			}
		}
	}

	private stopAll() {
		// stop all touched channels
		for (const t of Object.values(this._touched)) {
			const o = this._outputs.get(t.outputId);

			if (o) {
				MidiHelper.sendAllNotesOff(o, t.channel);
			}
		}
		this._touched = {};

		this._transport.tracks.forEach((t) => t.resetStep());
	}
}
