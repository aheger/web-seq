import { ITrack, ITransport } from './types';

export default class Transport implements ITransport {
	bpm: number = 125;
	divider: number = 4;

	tracks: ITrack[] = [];
}
